//
//  ViewController.h
//  WebViewTest
//
//  Created by U 2 on 12.01.15.
//  Copyright (c) 2015 LML. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *mainWebView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *undoButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *reloadButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *stopButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *redoButtonItem;





@end

