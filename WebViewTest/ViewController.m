//
//  ViewController.m
//  WebViewTest
//
//  Created by U 2 on 12.01.15.
//  Copyright (c) 2015 LML. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.mainWebView.delegate = self;
    self.mainWebView.scalesPageToFit = YES; //страницы под экран телефона.
    
    //NSString *localTXTUrl = [[NSBundle mainBundle] pathForResource: @"Сайты по IOS" ofType: @".rtf"];
    //NSString *localJPEGUrl = [[NSBundle mainBundle] pathForResource: @"14" ofType: @".jpg"];
    //NSURL *url = [NSURL URLWithString:@"http://placehold.it/600/9a3b4f"];
    //NSURL *url = [NSURL fileURLWithPath:localJPEGUrl isDirectory: NO];
    // NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //[self.mainWebView loadRequest:request];
    
    
//    UIImage *image = [UIImage imageNamed:@"14.jpg"];
//    NSData *data = UIImageJPEGRepresentation(image, 0.8);
//    [self.mainWebView loadData:data MIMEType: @"image/jpg" textEncodingName: @"NSUTF8StringEncoding" baseURL: nil];
//    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showGoogle:(id)sender {
   NSURL *url = [NSURL URLWithString:@"http://google.com"];
   NSURLRequest *request = [NSURLRequest requestWithURL:url];
   [self.mainWebView loadRequest:request];
}

- (IBAction)showJPEGFile:(id)sender {
    UIImage *image = [UIImage imageNamed:@"14.jpg"];
    NSData *data = UIImageJPEGRepresentation(image, 0.8);
    [self.mainWebView loadData:data MIMEType: @"image/jpg" textEncodingName: @"NSUTF8StringEncoding" baseURL: nil];
}

- (IBAction)showPDFFile:(id)sender {
    NSString *localPDFUrl = [[NSBundle mainBundle] pathForResource: @"photoshop_kocbyru" ofType: @".pdf"];
    NSURL *url = [NSURL fileURLWithPath:localPDFUrl isDirectory: NO];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.mainWebView loadRequest:request];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self updateButtons];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
}

- (void) updateButtons {
    self.undoButtonItem.enabled = self.mainWebView.canGoBack;
    self.redoButtonItem.enabled = self.mainWebView.canGoForward;
    self.stopButtonItem.enabled = self.mainWebView.loading;
}

@end
